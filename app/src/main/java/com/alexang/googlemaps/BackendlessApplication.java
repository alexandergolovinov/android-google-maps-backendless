package com.alexang.googlemaps;

import android.app.Application;

import com.backendless.Backendless;

public class BackendlessApplication extends Application {

    public static final String APP_ID = "D0E48CA9-FE1F-81EC-FF1D-62ED5EF99C00";
    public static final String ANDROID_API_KEY = "DD10390B-6F95-A528-FF34-6EC12E97C500";
    public static final String SERVER_URL = "https://api.backendless.com";

    @Override
    public void onCreate() {
        super.onCreate();
        Backendless.setUrl(BackendlessApplication.SERVER_URL);
        Backendless.initApp(getApplicationContext(), APP_ID
                , ANDROID_API_KEY);
    }
}
